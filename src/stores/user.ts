import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import type { Role } from '@/types/Role'
import { useMessageStore } from './message'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const users = ref<User[]>([])
  const roles = ref<Role[]>([])
  const initialUser: User & { files: File[] } = {
    email: '',
    password: '',
    fullName: '',
    image: 'noImage.jpg',
    gender: 'male',
    roles: [
      {
        id: 2,
        name: ' user'
      }
    ],
    files: []
  }

  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function saveUser() {
    try {
      /////////////// try catch to defind problem
      loadingStore.doLoad()
      ///////////
      console.log(editedUser.value)
      const user = editedUser.value
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user)
      }

      await getUsers()
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message) //show error message
      loadingStore.finish()
    }
  }
  async function deleteUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      const res = await userService.delUser(user)

      await getUsers()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  async function getRoles() {
    const res = await userService.getRoles()
    roles.value = res.data
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }
  return { users, getUsers, saveUser, deleteUser, editedUser, getUser, clearForm, getRoles, roles }
})
